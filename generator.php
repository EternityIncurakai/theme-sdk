<?php

/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


header('Content-Type: text/plain');
if(!defined('ROOT')) die;

// setup directory
mkdir(ROOT.'/'.$dir);
foreach($files as $file) file_put_contents(ROOT.'/'.$dir.'/'.$file, '');

// setup manifest
$man = [];
foreach($keys as $key) {
    if(!array_key_exists($key, $_POST)) die('error'); // add to this!
    $man[$key] = $_POST[$key]; 
}
file_put_contents(ROOT.'/'.$dir.'/manifest.json', json_encode($man, JSON_PRETTY_PRINT));

// setup icon
$icon = $_FILES['icon'];
$iconSize = getimagesize($icon['tmp_name']);
$iconType = $icon['type'];
$iconExt = pathinfo($icon['tmp_name'], PATHINFO_EXTENSION);

// check size of icon
if($iconSize[0] != 100 && $iconSize[1] != 100) die('error'); // add to this!
if(!in_array($iconType, $types)) die('error'); // add to this!
if(!in_array($iconExt, $exts)) die('error'); // add to this!

if(!move_uploaded_file($icon['tmp_name'], ROOT.'/'.$dir.'/icon.png')) die('error');
die;