<?php

/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

define('ROOT', dirname(__FILE__));

if(!extension_loaded('phar')) die('Must have phar installed');

$files = ['style.css', 'manifest.json', 'page.tpl', 'behavior.js'];
$keys = ['name', 'description', 'url', 'version', 'author', 'req_version', 'copyright'];
$types = ['image/png', 'image/jpeg', 'image/jpg', 'image/bmp', 'image/gif'];
$exts = ['png', 'jpeg', 'jpg', 'bmp', 'gif'];
$dir = 'theme';

// Include Generative file if requested.
if(strtolower($_SERVER['REQUEST_METHOD']) == 'post' && !empty($_POST)) {
    include ROOT.'/generator.php';
    die;
}