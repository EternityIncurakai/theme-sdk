<?php

/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
header('Content-Type: text/plain');
if(!defined('ROOT')) die('error');

$resource = (array)json_decode(utf8_encode(file_get_contents(ROOT.'/'.$dir.'/manifest.json')));
$exVersion = $_GET['ex_version'];
$thVersion = $_GET['theme_version'];
$reqVersion = $resource['req_version'];
$version = $resource['version'];

$exVersion = str_replace('α', 'a', $exVersion);
$exVersion = str_replace('β', 'b', $exVersion);

$thVersion = str_replace('α', 'a', $thVersion);
$thVersion = str_replace('β', 'b', $thVersion);

if(version_compare($exVersion, $reqVersion, '<')) die('incompatible'); // add to this!
if(version_compare($thVersion, $version, '<')) die('available: '.$version); // add to this!


