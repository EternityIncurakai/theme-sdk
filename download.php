<?php

/* 
 * EternityX
 * Copyright (C) 2014 Eternity Incurakai Studios
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
ini_set('phar.readonly', 0);

try {
    $phar = new Phar('extheme-sdk.phar');
    
    // check for requirements
    if(!is_dir(ROOT.'/'.$dir)) die('error'); // add to this!
    foreach($files as $file) 
        if(!file_exists(ROOT.'/'.$dir.'/'.$file)) die('error'); // add to this!
    
    // fetch metadata
    $phar->setMetdata((array)json_decode(utf8_encode(file_get_contents(ROOT.'/'.$dir.'/manifest.json'))));
    unlink(ROOT.'/'.$dir.'/manifest.json');
    
    $phar->buildFromDirectory(ROOT.'/'.$dir);
    $phar->compress();
    
    header('Content-Type: application/octet-stream');
    echo file_get_contents('extheme-sdk.phar');
    die;
    
} catch(PharException $e) {
    // add to this later.
}
